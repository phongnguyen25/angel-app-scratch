import { useState } from 'react'
import header from './assets/header.png'
import bottom from './assets/bottom.png'
import './App.scss'

function App() {
  const [state] = useState({
    totalInvestment: 6868,
    investmentLastWeek: 5000,
    totalInterest: 5000000000,
    interestLastWeek: 50000000,
    totalTopupAmount: 5000000000,
    topupAmountLastWeek: 1000000000,
    totalInvestor: 800,
    investorLastWeek: 25,
  })

  const {
    totalInvestment,
    investmentLastWeek,
    totalInterest,
    interestLastWeek,
    totalTopupAmount,
    topupAmountLastWeek,
    totalInvestor,
    investorLastWeek,
  } = state;

  return (
    <div className='container'>
      <div className="header">
        <img
          src={header}
          alt='header'
          className="image"
        />
      </div>

      <div className='content'>
        {/* TODO code day nha */}
        <p className="message">
          Hãy cùng nhìn lại các kết quả mà bạn đã góp phần trong khi chờ khoản đầu tư mới nhé!
        </p>
      </div>

      <div className="bottom">
        <img
          src={bottom}
          alt='bottom'
          className="image"
        />
      </div>
    </div>
  )
}

export default App
