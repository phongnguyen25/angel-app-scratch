## Getting Started
Install & Start:

- Install the dependencies:
```shell
npm install
```
- Start:

```shell
npm run dev
```
